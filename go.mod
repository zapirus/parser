module gitlab.com/zapirus/selenium

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/go-chi/chi v1.5.4
	github.com/ilyakaznacheev/cleanenv v1.4.2
	github.com/tebeka/selenium v0.9.9
	go.uber.org/zap v1.24.0
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	go.mongodb.org/mongo-driver v1.12.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4 // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
