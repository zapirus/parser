// Package classification Habr Career.
//
// Documentation of HabrParser API.
//
//	Schemes: http, https
//	Host: localhost:8000
//	BasePath: /
//	Version: 0.0.1
//
//	Consumes:
//	- application/json
//	- multipart/form-data
//
//	Produces:
//	- application/json
//
//	Security:
//	- basic
//
//
//	SecurityDefinitions:
//	  Bearer:
//	    type: apiKey
//	    name: Authorization
//	    in: header
//
// swagger:meta
package api

//go:generate swagger generate spec -o ../public/swagger.json --scan-models
