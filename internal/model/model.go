package model

type Vacancy struct {
	ID             int    `json:"id" db:"id"`
	Value          string `json:"value" db:"vacancy_id" bson:"_id"`
	Title          string `json:"title" db:"title" bson:"title"`
	Description    string `json:"description" db:"description" bson:"description"`
	EmploymentType string `json:"employmentType" db:"employment_type" bson:"employment_type"`
	Name           string `json:"name" db:"company" bson:"company"`
	DatePosted     string `json:"datePosted" db:"publish_date" bson:"date_posted"`
}
