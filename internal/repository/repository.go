package repository

import (
	"gitlab.com/zapirus/selenium/internal/config"
	"gitlab.com/zapirus/selenium/internal/model"
	"gitlab.com/zapirus/selenium/internal/repository/mongo"
	"gitlab.com/zapirus/selenium/internal/repository/postgres"
)

type RepoVacancy interface {
	Create(dto model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

func NewRepository(cfg config.Config) (RepoVacancy, error) {
	switch cfg.StorageType {
	case "postgres":
		res, err := postgres.NewRepository(cfg)
		return res, err
	case "mongo":
		res, err := mongo.NewRepository(cfg)
		return res, err
	}
	return nil, nil
}
