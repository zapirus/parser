package postgres

import (
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"

	"gitlab.com/zapirus/selenium/internal/config"
	"gitlab.com/zapirus/selenium/internal/model"
)

type VacancyStorage struct {
	db *sqlx.DB
}

func NewRepository(cfg config.Config) (*VacancyStorage, error) {
	time.Sleep(time.Second * 2)
	db, err := sqlx.Connect("pgx", cfg.Postgres.ConnString)
	if err != nil {
		return nil, err
	}
	return &VacancyStorage{db: db}, nil
}

func (v *VacancyStorage) Create(dto model.Vacancy) error {
	idVac, _ := strconv.Atoi(dto.Value)
	sql := `INSERT INTO vacancies (title, description,company,employment_type,vacancy_id,publish_date) 
				VALUES ($1, $2, $3, $4, $5, $6)`
	_, err := v.db.Exec(sql, dto.Title, dto.Description, dto.Name, dto.EmploymentType, idVac, dto.DatePosted)
	if err != nil {
		return err
	}
	return nil
}

func (v *VacancyStorage) GetByID(id int) (model.Vacancy, error) {
	res := model.Vacancy{}
	err := v.db.Get(&res, `SELECT * FROM vacancies WHERE vacancy_id = $1`, id)
	if err != nil {
		return model.Vacancy{}, err
	}
	return res, nil
}

func (v *VacancyStorage) GetList() ([]model.Vacancy, error) {
	var res []model.Vacancy
	err := v.db.Select(&res, "SELECT * FROM vacancies")
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (v *VacancyStorage) Delete(id int) error {
	_, err := v.db.Exec("DELETE FROM vacancies WHERE id=$1", id)
	if err != nil {
		return err
	}
	return nil

}
