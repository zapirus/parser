package mongo

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	_ "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	_ "go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"

	"gitlab.com/zapirus/selenium/internal/config"
	"gitlab.com/zapirus/selenium/internal/model"
	"gitlab.com/zapirus/selenium/pkg/logger"
)

var ctx = context.TODO()

type VacancyStorage struct {
	client     *mongo.Client
	collection *mongo.Collection
}

func NewRepository(cfg config.Config) (*VacancyStorage, error) {
	uri := fmt.Sprintf("mongodb://mongodb:%s/", cfg.MongoDB.Port)
	clientOptions := options.Client().ApplyURI(uri)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		logger.Error("Error connecting to MongoDB", zap.Error(err))
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		logger.Error("Error ping to MongoDB", zap.Error(err))
	}
	collection := client.Database("vac").Collection("vacancy")

	return &VacancyStorage{client: client, collection: collection}, nil
}

func (v *VacancyStorage) Create(dto model.Vacancy) error {
	_, err := v.collection.InsertOne(ctx, dto)
	if err != nil {
		return err
	}
	return nil
}

func (v *VacancyStorage) GetByID(id int) (model.Vacancy, error) {
	var vacancy model.Vacancy
	err := v.collection.FindOne(ctx, bson.M{"_id": id}).Decode(&vacancy)
	if err != nil {
		return model.Vacancy{}, fmt.Errorf("vacancy id %d not found", id)
	}
	return vacancy, nil
}

func (v *VacancyStorage) GetList() ([]model.Vacancy, error) {
	findOptions := options.Find()
	var vacancies []model.Vacancy
	cur, err := v.collection.Find(ctx, bson.D{{}}, findOptions)
	if err != nil {
		logger.Error("get vacancy error", zap.Error(err))
	}
	for cur.Next(ctx) {
		var elem model.Vacancy
		err := cur.Decode(&elem)
		if err != nil {
			logger.Error("get vacancy error", zap.Error(err))
		}
		vacancies = append(vacancies, elem)
	}
	return vacancies, nil
}

func (v *VacancyStorage) Delete(id int) error {
	_, err := v.collection.DeleteOne(ctx, bson.M{"_id": id})
	return err
}
