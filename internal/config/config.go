package config

import (
	"fmt"
	"log"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	DebugMode          string `env:"SERVER_DEBUG_MODE" env-description:"Debug mode logger" env-default:"yes"`
	HTTPServerHostPort string `env:"SERVER_HTTP_HOST_PORT" env-default:"localhost:8000"`
	StorageType        string `env:"STORAGE_TYPE" env-default:""`
	Postgres           Postgres
	MongoDB            MongoDB
	Names              Names
}

type Postgres struct {
	Username   string `env:"POSTGRES_USER" env-default:"postgres"`
	Password   string `env:"POSTGRES_PASSWORD" env-default:"postgres"`
	Host       string `env:"POSTGRES_HOST" env-default:"127.0.0.1"`
	Port       string `env:"POSTGRES_PORT" env-default:"5432"`
	ConnString string
}

type MongoDB struct {
	Host string `env:"MONGO_HOST" env-default:"127.0.0.1"`
	Port string `env:"MONGO_PORT" env-default:"27017"`
}

type Names struct {
	App string `env:"APP_NAME" env-default:"go-parser"`
	DB  string `env:"DB_NAME" env-default:"go-parser-db"`
}

var path = ".env"

func NewConfig() Config {
	storageType := os.Getenv("STORAGE_TYPE")
	log.Println("\t\tRead application configuration...")
	var cfg Config

	if err := cleanenv.ReadConfig(path, &cfg); err != nil {
		help, _ := cleanenv.GetDescription(&cfg, nil)
		log.Println(help)
		log.Fatalf("%s", err)
	}
	if storageType != "" {
		cfg.StorageType = storageType
	}
	cfg.Postgres.ConnString = fmt.Sprintf("postgres://%s:%s@%s:%s/postgres?sslmode=disable", cfg.Postgres.Username, cfg.Postgres.Password, cfg.Names.DB, cfg.Postgres.Port)

	log.Println(cfg.Postgres.ConnString)
	log.Printf("\t\tSTORAGE_TYPE=%s\n", cfg.StorageType)
	log.Println("\t\tGet configuration - OK!")

	return cfg
}
