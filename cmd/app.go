package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"go.uber.org/zap"

	"gitlab.com/zapirus/selenium/api"
	"gitlab.com/zapirus/selenium/internal/config"
	"gitlab.com/zapirus/selenium/internal/repository"
	"gitlab.com/zapirus/selenium/internal/service"
	"gitlab.com/zapirus/selenium/internal/transport"
	"gitlab.com/zapirus/selenium/pkg/graceful"
	"gitlab.com/zapirus/selenium/pkg/logger"
)

func main() {
	cfg := config.NewConfig()
	logger.Init(cfg.DebugMode)

	logger.Debug("Create router...")
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	repo, err := repository.NewRepository(cfg)
	if err != nil {
		logger.Fatal("Failed to create repository: ", zap.Error(err))
	}

	srv := service.NewService(repo)
	controller := transport.NewHandlers(srv)

	Register(r, controller)
	graceful.StartServer(r, cfg.HTTPServerHostPort)

}
func Register(r *chi.Mux, h *transport.Handler) {

	r.Post("/search", h.SearchVacancy)
	r.Post("/delete", h.DeleteVacancy)
	r.Post("/get", h.GetVacancy)
	r.Get("/list", h.ListVacancy)
	//SwaggerUI
	r.Get("/swagger", api.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})
}
