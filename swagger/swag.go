// Package api Package classification Habr Career.
//
// Documentation of HabrParser API.
//
//	Schemes: http, https
//	Host: localhost:8000
//	BasePath: /
//	Version: 0.0.1
//
//	Consumes:
//	- application/json
//	- multipart/form-data
//
//	Produces:
//	- application/json
//
//	Security:
//	- basic
//
//
//	SecurityDefinitions:
//	  Bearer:
//	    type: apiKey
//	    name: Authorization
//	    in: header
//
// swagger:meta
package main

//go:generate swagger generate spec -o swagger.json --scan-models
